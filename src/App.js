import {check,car} from './info';

function App() {
  return (
    <div className="App">
      <ul>
        {
          car.map((value)=>{
            return <li key={value.vID} >{value.vID} {value.make} {check(value)}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
